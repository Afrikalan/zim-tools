#!/usr/bin/python3
#
# Author: Julien Marin - projet Afrikalan  
# Contact: julien.marin@afrikalan.org
# Licence: GPL v3.0
#
# Usage: zimRecompress.py inputFile.zim outputFile.zim
# This will makes outputFile.zim a copy of inputFile.zim, recompressed in lzma for compatibility with legacy zim readers
# 
# 

import sys, pprint
import zimDerivate


##
##                 ###  Main part ###
##
## Change the lines below to make this script fit your needs.
##


if len(sys.argv)!=3:
    print ("usage: ", sys.argv[0], "inputFile.zim outputfile.zim")
    sys.exit(1) 
    
inZim=open(sys.argv[1], "rb")
outZim=open(sys.argv[2], "w+b")

print ('parsing original zim...')
parseInZim=zimDerivate.ParseZim(inZim)
if parseInZim.head['magicNumber']!=72173914:
    print ("The given input file is not a zim file.")
    

#for debugging only
#parseInZim.debugTitlePtr()
#pprint.pprint(vars(parseInZim))


## Let's go with the copy...
print ("Creating derivated zim...")
derivatedZim=zimDerivate.DerivatedZim(inZim, outZim, parseInZim)

## Set lzma compression
derivatedZim.convertCompress=4

print ("Copying clusters...")
derivatedZim.processCopy()

print ("updating checksum of derivated zim...");
derivatedZim.updateChecksum()
print('done!')

