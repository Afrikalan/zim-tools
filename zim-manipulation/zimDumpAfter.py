#!/usr/bin/python
# -*- coding: utf-8 -*-

###################################################
#
# zimDumpAfter.py [DEPRECATED]
# treat the dumped data from zimdump so it can
# be compressed again with zimwriterfs
#
# Author: Julien Marin - projet Afrikalan  
# Contact: julien.marin@afrikalan.org
# Licence: GPL v3.0
#
# usage: zimDumpAfter.py path/to/dump
#
# we made this projet in order to allow "editing"
# zim file. In our case, we wanted to rebuild the 
# bet without index..
#
# ** DEPRECATED: **
# We're not working anymore on this project
# we made zimDerivate.py witch offer a better way
# to make a modified copy of a zim file. 
#
#

import os, sys, re, urllib, shutil, time

class traiteRepDump:
    def __init__(self, rep):
        self.repBase=rep
        self.regExNsArticles=re.compile("^"+self.repBase+"/A($|/.*)")
        self.cacheRepsExistants=[]
        
        self.traiteRep(self.repBase)

    def traiteRep(self, actRep):
        listeFichiers=os.listdir(actRep)
        
        if self.regExNsArticles.match(actRep):
            boolRepArticle=True
        else:
            boolRepArticle=False
        
        for entree in listeFichiers:
            if os.path.isfile(actRep+"/"+entree):
                self.traiteFichier(actRep+"/"+entree, boolRepArticle)
            elif os.path.isdir(actRep+"/"+entree):
                self.traiteRep(actRep+"/"+entree)
                
    def traiteFichier(self, fichierOrig, boolRepArticle):
        
        if boolRepArticle:
            nvFichier=fichierOrig+".html"
            
        else:
            #nvFichier=urllib.unquote(fichierOrig)
            nvFichier=re.sub('(.)%2f', '\\1/', fichierOrig)
            
        nvFichier=nvFichier.replace(" ", "_")
        
        if(fichierOrig!=nvFichier):
            nvrep=os.path.split(nvFichier)[0]
            self.verifCreeRepArbo(nvrep)
            #print fichierOrig + "-->" + nvFichier
            shutil.move(fichierOrig, nvFichier)
            #os.rename(fichierOrig, nvFichier) #plus rapide que shutile.move, mais on doit rester sur le même système de fichiers
        
        
    
    def verifCreeRepArbo(self, arbo):
        if not arbo in self.cacheRepsExistants:
            if not os.path.isdir(arbo):
                os.makedirs(arbo)
            self.cacheRepsExistants.append(arbo)

if len(sys.argv) != 2:
    print 'Usage: ',sys.argv[0], ' path'
    exit()

start = time.time()
traiteRepDump(sys.argv[1])
end = time.time()
print "done in "+str(end-start)+" sec"
