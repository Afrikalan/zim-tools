#!/usr/bin/python3
#
# Author: Julien Marin - projet Afrikalan  
# Contact: julien.marin@afrikalan.org
# Licence: GPL v3.0
#
# Usage: zimCutIndex.py inputFile.zim outputFile.zim
# This will makes outputFile.zim a copy of inputFile.zim, without the index.
# 
# 

import sys, pprint
import zimDerivate


##
## Functions helping the specific task of removing the index
##

#Set the clusters we need to copy - in our case: all but the big cluster containing the index..
def genWantedClustersList(articlesList):
    clusterList=[]
    
    for article in articlesList:
        if article['mimeType']==zimDerivate.ParseZim.MIME_REDIRECT or article['mimeType']==zimDerivate.ParseZim.MIME_LINKTARGET or article['mimeType']==zimDerivate.ParseZim.MIME_DELETED:
          continue

        
        if article['nameSpace'] != 'X' and article['nameSpace'] != 'Z' and article['cluster'] not in clusterList: #and article['url']!="/fulltextIndex/xapian"
            clusterList.append(article['cluster'])
            
    return clusterList

#will update contents of M/Tags file...
def updateTags(txt):
  actTags=txt.split(';')
  newTags=[]
  for tag in actTags:
    if tag=="_ftindex:yes":
      tag="_ftindex:no"
    if tag!='_ftindex':
      newTags.append(tag)
  return(';'.join(newTags))



##
##                 ###  Main part ###
##
## Change the lines below to make this script fit your needs.
##


if len(sys.argv)<3:
    print ("usage: ", sys.argv[0], "inputFile.zim outputfile.zim [--recompress-lzma]")
    sys.exit(1) 
    
opt_recompressLzma=False
otherOpts=sys.argv[3:]
for opt in otherOpts:
  if opt=="--recompress-lzma":
    opt_recompressLzma=True
    
inZim=open(sys.argv[1], "rb")
outZim=open(sys.argv[2], "w+b")

print ('parsing original zim...')
parseInZim=zimDerivate.ParseZim(inZim)
if parseInZim.head['magicNumber']!=72173914:
    print ("The given input file is not a zim file.")
    

#for debugging only
#parseInZim.debugTitlePtr()
#pprint.pprint(vars(parseInZim))


## Let's go with the copy...
print ("Creating derivated zim...")
derivatedZim=zimDerivate.DerivatedZim(inZim, outZim, parseInZim)
if opt_recompressLzma:
  derivatedZim.convertCompress=4

print ("Determining what to copy...")
derivatedZim.wantedClusters=genWantedClustersList(zimDerivate.iterArticlesZim(parseInZim))
derivatedZim.registerArticleUpdate('M', 'Tags', updateTags)

print ("Copying clusters...")
derivatedZim.processCopy()

derivatedZim.deleteEntry('X', 'fulltext/xapian')
derivatedZim.deleteEntry('X', 'title/xapian')

print ("updating checksum of derivated zim...");
derivatedZim.updateChecksum()
print('done!')

