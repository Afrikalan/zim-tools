#!/usr/bin/python3
#
# Author: Julien Marin - projet Afrikalan  
# Contact: julien.marin@afrikalan.org
# Licence: GPL v3.0
#
# Usage: zimCutIndex.py inputFile.zim outputFile.zim
# (using this script as is will makes outputFile.zim a copy of inputFile.zim, without the index.)
# 
# 

import sys, pprint, re, os
import zimDerivate


##
## Functions helping the specific task of hacking the index to show random articles in it.
##

class InjectCodeWH:
  def injecterCode(self, html):
    #finHead=re.compile("</head>", re.IGNORECASE);
    #return("baaaaaaah")
    #return finHead.sub("\n"+self.codeWikihasard()+"\n"+self.launcherCode()+"\n</head>", html)
    return(html.replace("</head>", "\n"+self.codeWikihasard()+"\n"+self.launcherCode()+"\n</head>"))
  
  #search wikihasard file in many directories, then open it and return it content
  def openWhFile(self,fileName):
    scriptPath=os.path.dirname(os.path.realpath(__file__))
    dirList=['../wikiHasard', 'wikiHasard', '.', scriptPath+'/../wikiHasard', scriptPath+'/wikiHasard']
    for actdir in dirList:
      if os.path.isfile(actdir+"/"+fileName):
        return(open(actdir+"/"+fileName))
    raise NameError('file '+fileName+' not found!')
  
  def addslashes(self, s):
    d = {'"':'\\"', "'":"\\'", "\0":"\\\0", "\\":"\\\\"}
    return ''.join(d.get(c, c) for c in s)
  
  def config(self, confSel):
    print("selected config: "+confSel)
    if confSel=="wikipedia":
      self.conf_codeJsSpecifique='''
        var fctGenObjJqTexte=function(objJqContenuOrig){
          var objJqRet=objJqContenuOrig.find("#mf-section-0 > p");
          objJqRet.find(".mw-ref").remove();
        return( objJqRet ); }

        $("#bodyContent").css('display', 'flex');
        $("#bodyContent").css('flex-wrap', 'wrap');
        $("#bodyContent").css('max-width', '65rem');
        $("#mw-content-text").css('max-width', '40rem');

        $("<div id=conteneurResumArticles class=contResumeArticles ><h1>Quelques articles au hasard...</h1></div>").appendTo("#bodyContent");
        '''
      self.conf_optsWikihasard="{tailleDescr: 300, fctGenObjJqTexte: fctGenObjJqTexte, texteOrdreInverse: false}";
    elif confSel=="vikidia-officiel":
      self.conf_codeJsSpecifique='''
        var fctGenObjJqTexte=function(objJqContenuOrig){
        //(table.toccolours).remove();
          var objJqRet=objJqContenuOrig.find("#mw-content-text").children("p:lt(10)");
          objJqRet.find("style").remove();
          objJqRet.find("#sous_titre_h1").remove();
        return( objJqRet ); }

        //nettoyage titre
        $("p.titre-accueil").html("<span style='font-size: 2rem; font-weight: bold; margin-bottom: 2rem;'>Bienvenue sur vikidia hors-ligne!</span>");
        $("p.titre-accueil").next().remove();
        $("p.titre-accueil").next().remove();
        $("p.titre-accueil").next().remove();
        $("p.titre-accueil").next().remove();
        $("p.titre-accueil").prev().remove();
  
        //injection cadre
        $("#bodyContent").css('display', 'flex');
        $("#bodyContent").css('flex-wrap', 'wrap');
        $("#bodyContent").css('max-width', '65rem');
        $("#mw-content-text").css('max-width', '40rem');

        $("<div id=conteneurResumArticles class=contResumeArticles ><h1 style='font-family: sans;'>Quelques articles au hasard...</h1></div>").appendTo("#bodyContent");
        '''
      self.conf_optsWikihasard="{tailleDescr: 300, fctGenObjJqTexte: fctGenObjJqTexte, texteOrdreInverse: false, suffixeUrlArt: ''}";
    else:
      raise NameError('Pas de configuration pour ['+confSel+']') 
  
  def launcherCode(self):
    handleArt=self.openWhFile('articles.txt')
    articles=[]
    lineArt=handleArt.readline()
    while lineArt:
      articles.append("'"+self.addslashes(lineArt.strip())+"'");
      lineArt=handleArt.readline()
    
    return("""<script>
        arArticles=["""+", ".join(articles)+"""];
        
        urlBase='./';
	
        jQuery(function(){
        
        """+self.conf_codeJsSpecifique+"""
          
        var objWikiHazard=new WikiHazard(urlBase, arArticles, $(\"#conteneurResumArticles\"), """+self.conf_optsWikihasard+""");
        objWikiHazard.ajouterArticlesHasard(4);
        });
      </script>""")

  def codeWikihasard(self):
    handleJs=self.openWhFile('wikiHasard.js')
    handleJquery=self.openWhFile('jquery.min.js')
    handleCss=self.openWhFile('wikiHasard.css')
    return("<script>\n"+handleJquery.read()+"\n\n"+handleJs.read()+"\n</script>\n<style>\n"+handleCss.read()+"\n</style>")

#will update contents of index file...
def updateIndex(html):
  injecteur=InjectCodeWH()
  injecteur.config(prmPatchConfig)
  return(injecteur.injecterCode(html))



##
##                 ###  Main part ###
##
## Change the lines below to make this script fit your needs.
##


if len(sys.argv)!=3:
    print ("usage: ", sys.argv[0], "inputFile.zim outputfile.zim")
    sys.exit(1) 
    
inZim=open(sys.argv[1], "rb")
outZim=open(sys.argv[2], "w+b")

if "vikidia" in sys.argv[1]:
  prmPatchConfig="vikidia-officiel"
else:
  prmPatchConfig="wikipedia"


print ('parsing original zim...')
parseInZim=zimDerivate.ParseZim(inZim)
if parseInZim.head['magicNumber']!=72173914:
    print ("The given input file is not a zim file.")
    

#for debugging only
#parseInZim.debugTitlePtr()
#pprint.pprint(vars(parseInZim))
#pprint.pprint(parseInZim.head)
artAccueil=parseInZim.getEntryByUrlPtr(parseInZim.head['mainPage'])

## Let's go with the copy...
print ("Creating derivated zim...")
derivatedZim=zimDerivate.DerivatedZim(inZim, outZim, parseInZim)
#derivatedZim.registerArticleUpdate('A', 'Utilisateur:Popo_le_Chien_Kiwix.html', updateIndex)
#import pdb; pdb.set_trace()
print("will update ["+artAccueil['nameSpace']+"] "+artAccueil['url'])
derivatedZim.registerArticleUpdate(artAccueil['nameSpace'], artAccueil['url'], updateIndex)
#derivatedZim.registerArticleUpdate('A', 'index.htm', updateIndex)


print ("Copying clusters...")
derivatedZim.processCopy()
print ("updating checksum of derivated zim...");
derivatedZim.updateChecksum()
print('done!')

