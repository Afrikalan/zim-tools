#!/usr/bin/python3
#
# Author: Julien Marin - projet Afrikalan  
# Contact: julien.marin@afrikalan.org
# Licence: GPL v3.0
#
# Usage: zimCutIndex.py inputFile.zim outputFile.zim
# This will makes outputFile.zim a copy of inputFile.zim, without the index.
# 
# 

import sys, pprint
import zimDerivate

inZim=open(sys.argv[1], "rb")
outZim=open(sys.argv[2], "w+b")

print ('parsing original zim...')
parseInZim=zimDerivate.ParseZim(inZim)
if parseInZim.head['magicNumber']!=72173914:
    print ("The given input file is not a zim file.")
    
#pprint.pprint(parseInZim.clustersStarts)
pprint.pprint(parseInZim.head)
#pprint.pprint(parseInZim.urlPtrList)

parseInZim.debugTitlePtr()


outZim=open(sys.argv[2], "w+b")


## Let's go with the copy...
print ("Creating derivated zim...")
derivatedZim=zimDerivate.DerivatedZim(inZim, outZim, parseInZim)
print ("Copying clusters...")
derivatedZim.processCopy()
print ("updating checksum of derivated zim...");
derivatedZim.updateChecksum()
print('done!')

