# zim tools

Afriklan is spreading access to wikipedia in Mali and elsewhere. 

We made some work to help enhance slightly zim files to meet our needs, and share this work with anyone needing it.


## wikiHasard folder:

wikiHasard is a javascript tool allowing making a zim main page more dynamic by displaying random articles (title, an image and the first sentences). This way, the users see a diffrent page each time they open a zim of Wikipedia. For tehcnical reasons this script actually only work when the zim is served by kiwix-serve, not when viewed directly with Kiwix.

This folder contains the script and a demo zim file.


## zim-manipulation folder:

Theses tools helps the process of modifying/tweaking a zim file.

* zimDerivate.py: Copy a zim file, making changes on-the-fly, like removing index, or tweaking some files. This script has been developed in order to allow turning a indexed zim file into a file without index, but it may also help anyone needing some specific tweak to a zim file.
* zimDumpAfter.py: Process a dump created with zimdump so it can be compressed again with zimwriterfs. We don't use this tool anymore as we created zimDerivate.py to allow much faster uncompress/modify/compress work.

