# wikiHasard.js
Makes a zim main page more dynamic by displaying random articles into it

Author: Julien Marin - projet Afrikalan  
Contact: julien.marin@afrikalan.org
Licence: GPL v3.0

wikiHasard is a javascript tool allowing making a zim main page more dynamic by displaying random articles 
(title, an image and the first sentences). This way, the users see a diffrent page each time they open a zim 
of Wikipedia. For tehcnical reasons this script actually only work when the zim is served by kiwix-serve, 
not when viewed directly with Kiwix.

wikiHasard.js depends on jQuery, wich must be loaded as well to work.

## Usage:

### 1 load this script as well as jquery into the web page you want to use wikiHasard on 

when you load this file from a zim file, some javascript may be needed to dectect correct location see exemple zim file:

   <SCRIPT ID="js_Wikihazard"></SCRIPT>
   <SCRIPT ID="js_JQuery"></SCRIPT>
   <SCRIPT>
   document.getElementById("js_Wikihazard").src=window.location.protocol+"//"+window.location.host+window.location.pathname.match(/\/[^\/]*/)+"/-/wikiHasard.js";
   if (typeof jQuery=="undefined")
	    document.getElementById("js_JQuery").src=window.location.protocol+"//"+window.location.host+window.location.pathname.match(/\/[^\/]*/)+"/jquery.min.js";

   </SCRIPT>

### 2 - Create an instance of WikiHazard

in the demo zim, the instance is created that way:
var objWikiHazard=new WikiHazard(urlBase, arArticles, objJqConteneur, options);

you have to pass 3 parameters:
* urlBase: this is the base url that the script will use when readind the content of an article. the script will load article at urlBase+article_name. In the demo: urlBase=window.location.protocol+"//"+window.location.host+window.location.pathname.match(/\/[^\/]*/)+"/A/"; 
* arArticles an array containing a list of article name in wich wikiHasard will randomly pick some don't add .html to end of article names...
* objJqConteneur: a jquery object pointing to the container in wich the articles will be appened to by wikiHasard.

you may want to pass an object with more options as fourth parameter. This object can have the following properties:
* tailleDescr: number of caracters extracted from the article to display as "abstract" [default: 150];
* arSelecteursImag: an array with jquery selecteur that wikihasard will use to search an illustration for the article (first match will be used) [Default: ["[CLASS*=infobox] img", ".thumbinner img", "img.thumbimage"]]
* urlBaseImage: base url for loading image (in case it is not the same as urlBase) [default: value of urlBase]
* fctGenObjJqTexte: custom function that will receive a jquery object containing the whole article as parameter and will return a jquery object containing only the parts taht will be used to generate the "abstract" (which size is determined by tailleDescr parameter). This is usefull for selecting only the part of the article containing the text for the description without pollution of informatic code or others things. wikihasard provide his own function, provide your own if the defaut function give unwanted résults.
* texteOrdreInverse must be set to true when fctGenObjJqTexte return element in reverse order (this appends this is the case when fctGenObjJqTexte relies on prevAll (this is the case of this builtin fctGenObjJqTexte so the default is true [default: true]
* objReplaceNomArticlesPourUrl: an object in form of [search: replace] wich will be used to perform caracters replacement in articles names when used in url [default: {' ': '_'}]
* limiteTaillePetite: if article width is under the given given size in pixel, a specific CSS class (petiteTaille) will be added. This is obsoblete and should be handled with proper CSS. [default: 250];
* remplaceErr: boolean: if come article fails to load, should wikiHasard try to load another article for replacint it [default: true]

### include 


TODO: rewrite this script with ES6 to make it cleaner.
