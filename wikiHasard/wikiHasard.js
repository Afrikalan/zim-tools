///////////////////////////////////////////////////////////////////////////////////////////////
//
// wikiHasard.js
// Makes a zim main page more dynamic by displaying random articles into it
//
// Author: Julien Marin - projet Afrikalan  
// Contact: julien.marin@afrikalan.org
// Licence: GPL v3.0
//
// wikiHasard is a javascript tool allowing making a zim main page more dynamic by displaying random articles 
// (title, an image and the first sentences). This way, the users see a diffrent page each time they open a zim 
// of Wikipedia. For tehcnical reasons this script actually only work when the zim is served by kiwix-serve, 
// not when viewed directly with Kiwix.
//
// wikiHasard.js depends on jQuery, wich must be loaded as well to work.
//
// See Readme.md for usage informations.


//
// Crée un texte "animé" pour signifier à l'utilis
//
// -------------------
//
// Create an "animated" text to tell the user some article is loading
// Usage: create object, and call marche() when need to start animation. Marche() returns a jquery object 
// you need to append() it where you need to display, call arret() and remove() the jQuery 
//
class AnimChargement
{
	constructor()
	{
		this.compteurTick=0;
		this.objJqAnim=$("<SPAN CLASS=animChargement>Chargement</SPAN>");
	}
	
	evtTick()
	{
		this.compteurTick++;
		this.objJqAnim.text("Chargement....".slice(0, 10+(this.compteurTick%4)))
	}
	
	marche()
	{
		this.handleTimeout=setInterval(this.evtTick.bind(this), 250);
		return(this.objJqAnim);
	}
	
	arret()
	{
		clearInterval(this.handleTimeout);
	}
}






class ArticleWikiHazard
{
	constructor(urlBase, idArticle, cbFinCharge, options)
	{
		this.urlBase=urlBase;
		this.idArticle=idArticle;
		this.cbFinCharge=cbFinCharge;
		this.options=options;
    
		this.initialiser();
		this.afficher();
  }

  initialiser()
  {
    this.objJqContenu=$("<DIV CLASS=resumeArticle></DIV>");
    
    this.idArticlePourUrl=this.idArticle;
    for (var i in this.options.objReplaceNomArticlesPourUrl)
      this.idArticlePourUrl=this.idArticlePourUrl.replace(new RegExp("/"+i+"/", "g"), this.options.objReplaceNomArticlesPourUrl[i]);

    this.url=this.urlBase+this.idArticlePourUrl+this.options.suffixeUrlArt;
    this.objAnimChargement=new AnimChargement();
  }
		
  evtPageRecue(contenu)
  {
    this.objJqAnimChargement.remove();
    this.objAnimChargement.arret();
    
    this.objJqContenuOrig=$(contenu.replace(/SRC=/gi, "dsblsrc="));
    
    //recherche d'une image
    var objJqImg=[];
    for (var i in this.options.arSelecteursImage)			  
      if (objJqImg.length==0) objJqImg=this.objJqContenuOrig.find(this.options.arSelecteursImage[i]).first();
    
    objJqImg.attr("src", this.options.urlBaseImage+objJqImg.attr("dsblsrc"));
    
    
    //insertion du résumé
    var objJqResume=this.options.fctGenObjJqTexte(this.objJqContenuOrig);

    var texte='';
    
    if (this.options.texteOrdreInverse) objJqResume.each(function(i,elt){texte=$(elt).text()+" "+texte}); //remet dans l'ordre car prevAll a tout retourné en ordre inverse...
    else objJqResume.each(function(i,elt){texte=texte+" "+$(elt).text()}); 

    
    if (texte.length>this.options.tailleDescr)
    {
      texte=texte.substring(0, this.options.tailleDescr);
      texte=texte.substring(0, texte.lastIndexOf(" "));
      texte=texte+" (...)";
    }
    
    //construction de l'affichage
    
    $("<H2><A HREF=\""+this.url+"\">"+this.idArticle.replace(/_/, " ")+"</A></H2>").appendTo(this.objJqContenu);
    if (objJqImg.length!=0) objJqImg.appendTo(this.objJqContenu);
    $("<P>"+texte+"</P>").appendTo(this.objJqContenu);
    $("<A CLASS=lienVersArticleComplet HREF=\""+this.url+"\">Lire l'article complet</A>").appendTo(this.objJqContenu);
    
    
    this.cbFinCharge(true);
  }
		
  evtErreurCharge()
  {
    this.objJqContenu.remove();
    this.cbFinCharge(false);
  }
  
  
  afficher()
  {
    this.objJqAnimChargement=this.objAnimChargement.marche();
    this.objJqAnimChargement.appendTo(this.objJqContenu);
    $.ajax(this.url, {success: this.evtPageRecue.bind(this), error: this.evtErreurCharge.bind(this) });
  }

  getObjJqArticle(){ return(this.objJqContenu); }
}





class WikiHazard
{
  constructor(urlBase, arArticles, objJqConteneur, options)
  {
    this.urlBase=urlBase;
    this.arArticles=arArticles;
    this.objJqConteneur=objJqConteneur;
    this.options=options;
    
    //valeurs par défaut
    if (typeof this.options=="undefined")			this.options={};
    if (typeof this.options.tailleDescr=="undefined")	this.options.tailleDescr=150;
    if (typeof this.options.arSelecteursImage=="undefined") this.options.arSelecteursImage=["[CLASS*=infobox] img", ".thumbinner img", "img.thumbimage"];
    if (typeof this.options.urlBaseImage=="undefined") 	this.options.urlBaseImage=this.urlBase;
    if (typeof this.options.fctGenObjJqTexte=="undefined") this.options.fctGenObjJqTexte=function(objJqContenuOrig){ return(objJqContenuOrig.find("h2").first().prevAll("p")); }
    if (typeof this.options.texteOrdreInverse=="undefined") this.options.texteOrdreInverse=true;
    if (typeof this.options.objReplaceNomArticlesPourUrl=="undefined") this.options.objReplaceNomArticlesPourUrl={' ': '_'};
    if (typeof this.options.remplaceErr=="undefined") 	this.options.remplaceErr=true;
    if (typeof this.options.suffixeUrlArt=="undefined") 	this.options.suffixeUrlArt="";
    
    this.initialiser();
  }

	
	
	initialiser()
	{
		this.nbArticlesAffiches=0;
		this.nbArticlesAAfficher=0;
	}
	
	ajouterArticle(idArticle)
	{
		let objArticle=new ArticleWikiHazard(this.urlBase, idArticle, this.evtArticleFinCharge.bind(this), this.options);
		objArticle.getObjJqArticle().appendTo(this.objJqConteneur);
		this.nbArticlesAffiches++;
	}

	processQueue()
  {
    if (this.nbArticlesAffiches<this.nbArticlesAAfficher)
      this.ajouterArticle(this.arArticles[Math.floor(Math.random()*this.arArticles.length)]);
  }
	
	evtArticleFinCharge(boolSuccess)
  {
    if (boolSuccess)
      this.processQueue();
    else
    {
      if (this.options.remplaceErr)
        this.ajouterArticlesHasard(1);
    }
  }
	
	ajouterArticlesHasard(nbArticles)
	{
		this.nbArticlesAAfficher+=nbArticles;
		this.processQueue();
	}

	ajouterArticlesPlage(debut, fin)
	{
		for (var i=debut; ((i<fin) && (i<this.arArticles.length)) ; i++)
			this.ajouterArticle(this.arArticles[i]);
	}
}
